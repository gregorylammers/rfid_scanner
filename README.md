# RFID_Scanner
An application writen in Flutter to read/write RFID-tags

## Download Flutter
- Download the zip 
https://flutter.io/docs/get-started/install/macos
- Extract the zip file in the desired location, for example:
```
$ unzip ~/Desktop/flutter_macos_v1.0.0-stable.zip
```

## Update your PATH
- Open the terminal and type cd
- Create a .bash_profile 
``` 
$ vim .bash_profile 
```
- Press i to insert path in .bash_profile 
```
$ export PATH=~/Desktop/flutter/bin:$PATH 
```
- Press esc, then press : and type wq (write and quit) to save
- Close the terminal and quit the terminal
- Restart the terminal 
- Type flutt and press tab
- When it auto completes after hitting the tab, then your path is set correctly

## Run the project
- Clone the project
```
git clone https://gregorylammers@bitbucket.org/gregorylammers/rfid_scanner.git
```
- Run the following command to see if there are any dependencies you need to install to complete the setup:

```
$ flutter doctor
```
- Run the project
```
$ cd rfid_scanner
$ flutter run
```

# Author

- Gregory Lammers

